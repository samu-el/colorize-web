import LModel from "./api";
import ClientSession from "./client-session.js";

const pluralName = "Accounts";

class User {
  static login = (email, password) => {
    const url = pluralName + "/login?include=roles";
    if (email && password) {
      return LModel.create(url, {
        email: email,
        password: password,
        ttl: 300000000
      }).then(
        response => {
          ClientSession.storeAuth(response.data, err => {
            return err ? console.error("cannot save session") : "";
          });
          return {
            success: true,
            message: "Logged in successfuly",
            user: response.data
          };
        },
        error => {
          if (error.response) {
            if (error.response.status === 401) {
              return {
                error: true,
                message: "Invalid username or password"
              };
            }
            return {
              error: true,
              message: "Oops error occured please. Try Again"
            };
          }
          return {
            error: true,
            message: "Error: Not connected"
          };
        }
      );
    }
  };

  static reset = email => {
    if (email) {
      return LModel.create("Accounts/reset", { email: email }).then(
        response => {
          return {
            success: true,
            message: "Email sent Successfully",
            user: response.data
          };
        },
        error => {
          if (error.response) {
            if (error.response.status === 401) {
              return {
                error: true,
                message: "user email is not found"
              };
            }
            return {
              error: true,
              message: "Oops error occurred please. Try Again"
            };
          }
          return {
            error: true,
            message: "Error: Not connected"
          };
        }
      );
    }
  };
  static changePassword = (password, accessToken) => {
    ClientSession.removeAuth(err => {});
    if (password) {
      return LModel.create("accounts/reset-password" + accessToken, {
        newPassword: password
      })
        .then(
          response => {
            return {
              success: true,
              message: "Password changed successfully",
              user: response.data
            };
          },
          error => {
            return {
              error: true,
              message: "Oops error occurred please. Try Again"
            };
          }
        )
        .catch(error => {
          return {
            error: true,
            message: "Oops error Occurred please. Try Again"
          };
        });
    }
  };

  static register = values => {
    if (values.email) {
      return LModel.create(pluralName, values)
        .then(
          response => {
            return {
              success: true,
              message:
                "Registered successfully! Check email to confirm account",
              user: response.data
            };
          },
          error => {
            if (error.response) {
              if (error.response.status === 422) {
                return {
                  error: true,
                  message: error.response.data.error.message,
                  codes: error.response.data.error.details.codes
                };
              }
            } else {
              return {
                error: true,
                message: "Oops error occurred please. Try Again"
              };
            }
          }
        )
        .catch(error => {
          return {
            error: true,
            message: "Oops error Occurred please. Try Again"
          };
        });
    }
  };

  static logout = () => {
    ClientSession.getAccessToken(function(isLoggedIn, authData) {
      if (isLoggedIn && authData != null) {
        LModel.create("Accounts/logout", {})
          .then(response => {
            ClientSession.removeAuth(err => {});
            window.location = "#/login";
          })
          .catch(err => {
            ClientSession.removeAuth(err => {});
            window.location = "#/login";
          });
      }
    });
  };
}

export default User;
