import PropTypes from 'prop-types';
import React from 'react';
import classNames from "classnames";

import withStyles from "@material-ui/core/styles/withStyles";
import Gallery from "react-grid-gallery";
import landingPageStyle from "assets/jss/material-kit-react/views/landingPage.jsx";
import Header from "components/Header/Header.jsx";
import HeaderLinks from "components/Header/HeaderLinks.jsx";
import Parallax from "components/Parallax/Parallax.jsx";


import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import LModel from '../../services/api';
import ClientSession from '../../services/client-session';


class AlbumGallery extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            albums: [],//this.props.images,
            images: [],//this.props.images,
            selectAllChecked: false
        };

        this.onSelectImage = this.onSelectImage.bind(this);
        this.getSelectedImages = this.getSelectedImages.bind(this);
        this.onClickSelectAll = this.onClickSelectAll.bind(this);
    }

    componentDidMount(){
        ClientSession.getAuth((err, value)=>{
            if(value.accountId){
                let filter = `filter={"include":"images", "where":{"accountId":"${value.accountId}"}}`
                LModel.find("Albums", null, filter)
                    .then(response => {
                        let albumObjs = []
                        response.data.map(album => {
                                let images = []
                                if(album.images){
                                    album.images.map(imageObj => {
    
                                        let imgG = {
                                            src: imageObj.gray,
                                            thumbnail: imageObj.gray,
                                            thumbnailWidth: 256,
                                            thumbnailHeight: 256,
                                            tags: [{value: imageObj.album? imageObj.album.name:"", title: imageObj.album? imageObj.album.name:""}],
                                        }
                                        let imgC = {
                                            src: imageObj.color,
                                            thumbnail: imageObj.color,
                                            thumbnailWidth: 256,
                                            thumbnailHeight: 256,
                                            tags: [{value: imageObj.album? imageObj.album.name:"", title: imageObj.album? imageObj.album.name:""}],
                                        }
                                        images.push(imgG, imgC)
                                    })
                                }
                                album.images = images
                                albumObjs.push(album)
                        })
                        this.setState({albums: albumObjs})
                        console.log(response.data);
                        
                    })
            }
        })
    }

    allImagesSelected (images){
        var f = images.filter(
            function (img) {
                return img.isSelected === true;
            }
        );
        return f.length === images.length;
    }

    onSelectImage (index, image) {
        var images = this.state.images.slice();
        var img = images[index];
        if(img.hasOwnProperty("isSelected"))
            img.isSelected = !img.isSelected;
        else
            img.isSelected = true;

        this.setState({
            images: images
        });

        if(this.allImagesSelected(images)){
            this.setState({
                selectAllChecked: true
            });
        }
        else {
            this.setState({
                selectAllChecked: false
            });
        }
    }

    getSelectedImages () {
        var selected = [];
        for(var i = 0; i < this.state.images.length; i++)
            if(this.state.images[i].isSelected === true)
                selected.push(i);
        return selected;
    }

    onClickSelectAll () {
        var selectAllChecked = !this.state.selectAllChecked;
        this.setState({
            selectAllChecked: selectAllChecked
        });

        var images = this.state.images.slice();
        if(selectAllChecked){
            for(let i = 0; i < this.state.images.length; i++)
                images[i].isSelected = true;
        }
        else {
            for(let i = 0; i < this.state.images.length; i++)
                images[i].isSelected = false;

        }
        this.setState({
            images: images
        });
    }

    render () {
        const { classes, ...rest } = this.props;

        return (
            <div>
            <Header
          color="transparent"
          brand="Colorize"
          rightLinks={<HeaderLinks />}
          fixed
          changeColorOnScroll={{
            height: 200,
            color: "white"
          }}
          {...rest}
        />
        <Parallax small filter image={require("assets/img/profile-bg.jpg")} style={{height:'200px'}}/>
        <div className={classNames(classes.main, classes.mainRaised)}>
          <div className={classes.container}>
      
            <Card>
                <div className='m-2' style={{display:"flex", justifyContent:""}}>
                    <h2 style={{color: "#3C4858",
                                margin:"1.75rem 0 0.875rem",
                                marginTop: "30px",
                                minHeight: "32px",
                                fontWeight: "700",
                                fontFamily: "Roboto Slab, Times New Roman, serif",
                                textDecoration: "none",}}
                    >
                      My Albums
                    </h2>
                  </div>
                <CardBody>
                <div>
                    {/* <CheckButton
                        index={0}
                        isSelected={this.state.selectAllChecked}
                        onClick={this.onClickSelectAll}
                        parentHover={true}
                        color={"rgba(0,0,0,0.54)"}
                        selectedColor={"#4285f4"}
                        hoverColor={"rgba(0,0,0,0.54)"}/> */}
                        {/* <div style={{
                            height: "36px",
                            display: "flex",
                            alignItems: "center"
                        }}>
                        select all
                        </div> */}
                    {/* <div style={{
                        padding: "2px",
                        color: "#666"
                    }}>Selected: {this.getSelectedImages().toString()}</div> */}

                    {this.state.albums.map( album => {
                        return(
                            <div>
                            <h3>{album.name}</h3>
                            <div style={{
                                display: "block",
                                minHeight: "1px",
                                width: "100%",
                                border: "1px solid #ddd",
                                overflow: "auto"}}>
                                    {album.images && album.images.length?
                                        <Gallery
                                            images={album.images}
                                            onSelectImage={this.onSelectImage}
                                            showLightboxThumbnails={true}/>
                                        : ""
                                    }
                            </div>
                            </div>
                        )

                    })}
                </div>
            </CardBody>
        </Card>
        </div>
        </div>
        </div>
        );
    }
}

AlbumGallery.propTypes = {
    images: PropTypes.arrayOf(
        PropTypes.shape({
            src: PropTypes.string.isRequired,
            thumbnail: PropTypes.string.isRequired,
            srcset: PropTypes.array,
            caption: PropTypes.string,
            thumbnailWidth: PropTypes.number.isRequired,
            thumbnailHeight: PropTypes.number.isRequired,
            isSelected: PropTypes.bool
        })
    ).isRequired
};

AlbumGallery.defaultProps = {
    images:[
    ]
};

export default withStyles(landingPageStyle)(AlbumGallery);