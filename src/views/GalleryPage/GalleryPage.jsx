import PropTypes from 'prop-types';
import React from 'react';
import withStyles from "@material-ui/core/styles/withStyles";
import Gallery from "react-grid-gallery";
import productStyle from "assets/jss/material-kit-react/views/landingPageSections/productStyle.jsx";

import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import LModel from '../../services/api';
import ClientSession from '../../services/client-session';


class ImageGallery extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            images: [],//this.props.images,
            selectAllChecked: false
        };

        this.onSelectImage = this.onSelectImage.bind(this);
        this.getSelectedImages = this.getSelectedImages.bind(this);
        this.onClickSelectAll = this.onClickSelectAll.bind(this);
    }

    componentDidMount(){
        ClientSession.getAuth((err, value)=>{
            if(value.accountId){
                let filter = `filter={"include":["album","favoritedAccounts"], "where":{"accountId":"${value.accountId}"}}`
                LModel.find("Images", null, filter)
                    .then(response => {
                        let images = []
                        response.data.map(imageObj => {
                            let imgG = {
                                src: imageObj.gray,
                                thumbnail: imageObj.gray,
                                thumbnailWidth: 256,
                                thumbnailHeight: 256,
                                tags: [{value: imageObj.album? imageObj.album.name:"", title: imageObj.album? imageObj.album.name:""}],
                            }
                            let imgC = {
                                src: imageObj.color,
                                thumbnail: imageObj.color,
                                thumbnailWidth: 256,
                                thumbnailHeight: 256,
                                tags: [{value: imageObj.album? imageObj.album.name:"", title: imageObj.album? imageObj.album.name:""}],
                            }
                            images.push(imgG, imgC)
                        })
                        this.setState({images})
                        console.log(response.data);
                        
                    })
            }
        })
    }

    allImagesSelected (images){
        var f = images.filter(
            function (img) {
                return img.isSelected === true;
            }
        );
        return f.length === images.length;
    }

    onSelectImage (index, image) {
        var images = this.state.images.slice();
        var img = images[index];
        if(img.hasOwnProperty("isSelected"))
            img.isSelected = !img.isSelected;
        else
            img.isSelected = true;

        this.setState({
            images: images
        });

        if(this.allImagesSelected(images)){
            this.setState({
                selectAllChecked: true
            });
        }
        else {
            this.setState({
                selectAllChecked: false
            });
        }
    }

    getSelectedImages () {
        var selected = [];
        for(var i = 0; i < this.state.images.length; i++)
            if(this.state.images[i].isSelected === true)
                selected.push(i);
        return selected;
    }

    onClickSelectAll () {
        var selectAllChecked = !this.state.selectAllChecked;
        this.setState({
            selectAllChecked: selectAllChecked
        });

        var images = this.state.images.slice();
        if(selectAllChecked){
            for(let i = 0; i < this.state.images.length; i++)
                images[i].isSelected = true;
        }
        else {
            for(let i = 0; i < this.state.images.length; i++)
                images[i].isSelected = false;

        }
        this.setState({
            images: images
        });
    }

    render () {
        return (
            <Card>
                <div style={{display:"flex", justifyContent:"center"}}>
                    <h2 style={{color: "#3C4858",
                                margin:"1.75rem 0 0.875rem",
                                marginTop: "30px",
                                minHeight: "32px",
                                fontWeight: "700",
                                fontFamily: "Roboto Slab, Times New Roman, serif",
                                textDecoration: "none",}}
                    >
                      Gallery
                    </h2>
                  </div>
                <CardBody>
                <div>
                    {/* <CheckButton
                        index={0}
                        isSelected={this.state.selectAllChecked}
                        onClick={this.onClickSelectAll}
                        parentHover={true}
                        color={"rgba(0,0,0,0.54)"}
                        selectedColor={"#4285f4"}
                        hoverColor={"rgba(0,0,0,0.54)"}/> */}
                        {/* <div style={{
                            height: "36px",
                            display: "flex",
                            alignItems: "center"
                        }}>
                        select all
                        </div> */}
                    <div style={{
                        padding: "2px",
                        color: "#666"
                    }}>Selected: {this.getSelectedImages().toString()}</div>
                    <div style={{
                        display: "block",
                        minHeight: "1px",
                        width: "100%",
                        border: "1px solid #ddd",
                        overflow: "auto"}}>
                            <Gallery
                                images={this.state.images}
                                onSelectImage={this.onSelectImage}
                                showLightboxThumbnails={true}/>
                    </div>
                </div>
            </CardBody>
        </Card>
        );
    }
}

ImageGallery.propTypes = {
    images: PropTypes.arrayOf(
        PropTypes.shape({
            src: PropTypes.string.isRequired,
            thumbnail: PropTypes.string.isRequired,
            srcset: PropTypes.array,
            caption: PropTypes.string,
            thumbnailWidth: PropTypes.number.isRequired,
            thumbnailHeight: PropTypes.number.isRequired,
            isSelected: PropTypes.bool
        })
    ).isRequired
};

ImageGallery.defaultProps = {
    images:[
        {
            src:"https://picsum.photos/1080/?image=815",
            thumbnail: "https://picsum.photos/320/?image=815",
            thumbnailWidth: 320,
            thumbnailHeight: 174,
            tags: [{value: "Nature", title: "Nature"}, {value: "Flora", title: "Flora"}],
            caption: "After Rain (Jeshu John - designerspics.com)"
        },
        {
            src:"https://picsum.photos/1080/?image=201",
            thumbnail: "https://picsum.photos/320/?image=201",
            thumbnailWidth: 320,
            thumbnailHeight: 212,
            caption: "Boats (Jeshu John - designerspics.com)"
        },
        {
            src: "https://picsum.photos/1080/?image=190",
            thumbnail: "https://picsum.photos/320/?image=190",
            thumbnailWidth: 320,
            thumbnailHeight: 213,
            caption: "Red Apples with other Red Fruit (foodiesfeed.com)"
        }, {
            src:"https://picsum.photos/1080/?image=120",
            thumbnail: "https://picsum.photos/320/?image=120",
            thumbnailWidth: 257,
            thumbnailHeight: 320,
            caption: "A photo by 贝莉儿 NG. (unsplash.com)"
        },
        {
            src: "https://picsum.photos/1080/?image=110",
            thumbnail: "https://picsum.photos/320/?image=110",
            thumbnailWidth: 320,
            thumbnailHeight: 183,
            caption: "37H (gratispgraphy.com)"
        },
        {
            src: "https://picsum.photos/1080/?image=199",
            thumbnail: "https://picsum.photos/320/?image=199",
            thumbnailWidth: 320,
            thumbnailHeight: 190,
            caption: "286H (gratisography.com)"
        },
        {
            src: "https://picsum.photos/1080/?image=140",
            thumbnail: "https://picsum.photos/320/?image=140",
            thumbnailWidth: 320,
            thumbnailHeight: 148,
            tags: [{value: "People", title: "People"}],
            caption: "315H (gratisography.com)"
        },
        {
            src: "https://picsum.photos/1080/?image=90",
            thumbnail: "https://picsum.photos/320/?image=90",
            thumbnailWidth: 320,
            thumbnailHeight: 213,
            caption: "201H (gratisography.com)"
        },
        {
            src: "https://picsum.photos/1080/?image=610",
            alt: "Big Ben - London",
            thumbnail: "https://picsum.photos/320/?image=610",
            thumbnailWidth: 248,
            thumbnailHeight: 320,
            caption: "Big Ben (Tom Eversley - isorepublic.com)"
        },
        {
            src: "https://picsum.photos/1080/?image=787",
            alt: "Red Zone - Paris",
            thumbnail: "https://picsum.photos/320/?image=787",
            thumbnailWidth: 320,
            thumbnailHeight: 113,
            tags: [{value: "People", title: "People"}],
            caption: "Red Zone - Paris (Tom Eversley - isorepublic.com)"
        },
        {
            src: "https://picsum.photos/1080/?image=700",
            alt: "Wood Glass",
            thumbnail: "https://picsum.photos/320/?image=700",
            thumbnailWidth: 313,
            thumbnailHeight: 320,
            caption: "Wood Glass (Tom Eversley - isorepublic.com)"
        },
        {
            src: "https://picsum.photos/1080/?image=900",
            alt: "Cosmos Flower",
            thumbnail: "https://picsum.photos/320/?image=900",
            thumbnailWidth: 320,
            thumbnailHeight: 213,
            caption: "Cosmos Flower Macro (Tom Eversley - isorepublic.com)"
        },
        {
            src: "https://picsum.photos/1080/?image=230",
            thumbnail: "https://picsum.photos/320/?image=230",
            thumbnailWidth: 271,
            thumbnailHeight: 320,
            caption: "Orange Macro (Tom Eversley - isorepublic.com)"
        },
        {
            src: "https://picsum.photos/1080/?image=390",
            thumbnail: "https://picsum.photos/320/?image=390",
            thumbnailWidth: 320,
            thumbnailHeight: 213,
            tags: [{value: "Nature", title: "Nature"}, {value: "People", title: "People"}],
            caption: "Surfer Sunset (Tom Eversley - isorepublic.com)"
        },
        {
            src: "https://picsum.photos/1080/?image=431",
            thumbnail: "https://picsum.photos/320/?image=431",
            thumbnailWidth: 320,
            thumbnailHeight: 213,
            tags: [{value: "People", title: "People"}, {value: "Sport", title: "Sport"}],
            caption: "Man on BMX (Tom Eversley - isorepublic.com)"
        },
        {
            src: "https://picsum.photos/1080/?image=521",
            thumbnail: "https://picsum.photos/320/?image=521",
            thumbnailWidth: 320,
            thumbnailHeight: 213,
            caption: "Time to Think (Tom Eversley - isorepublic.com)"
        },
        {
            src: "https://picsum.photos/1080/?image=522",
            thumbnail: "https://picsum.photos/320/?image=522",
            thumbnailWidth: 320,
            thumbnailHeight: 215,
            tags: [{value: "People", title: "People"}],
            caption: "Untitled (moveast.me)"
        },
    ]
};

export default withStyles(productStyle)(ImageGallery);