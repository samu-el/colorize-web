import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";


// @material-ui/icons

// core components
import Header from "components/Header/Header.jsx";
import Footer from "components/Footer/Footer.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import HeaderLinks from "components/Header/HeaderLinks.jsx";
import Parallax from "components/Parallax/Parallax.jsx";

import landingPageStyle from "assets/jss/material-kit-react/views/landingPage.jsx";

import { Input } from "@material-ui/core";
import ImageGallery from "../GalleryPage/GalleryPage.jsx";
import ClientSession from 'services/client-session.js'
import ColorizePage from "./ColorizePage.jsx";

import LModel from "../../services/api";
import { message, Spin, Icon } from "antd";
import MobileStoreButton from "react-mobile-store-button";


const dashboardRoutes = [];

class LandingPage extends React.Component {
  constructor(props){
    super(props)

    this.state  = {
      isLoggedIn : false,
      fileList: [],
      currentImage: "",
      colorImage: "",
      colorImageEdited: "",
      dataImage: "",
      activeTab: 0,
      uploading: false
    }
  }

  componentDidMount() {

  }

 
  handleChangeFile = (e) => {
    console.log(e.target.files)
    if(e.target.files && e.target.files.length && e.target.files[0].type.startsWith('image')){
      console.log("uploading")
      this.setState({uploading:true,activeTab:0})
      LModel.upload('images', e.target.files)
        .then(response => {
          var files = response.data.result.files.data;
            if (files.length) {
              var file = files[0];
              let image = `${LModel.API_BASE_URL}Containers/${
                file.container
              }/download/${file.name}`;
              LModel.create('Colorizations/auto', {image:file.name})
                  .then( response2 => {
                    console.log(response2)
                    LModel.create('Images', {gray: image, color: response2.data.colorImage})
                      .then( resp3 => {
                        this.setState({currentImage: image, colorImage: response2.data.colorImage, activeTab:1, uploading: false})
                      }) .catch(err => {
                        this.setState({currentImage: image, colorImage: response2.data.colorImage, activeTab:1, uploading: false})
                        console.log(err)
                      })
                  }) .catch(err => {
                    this.setState({uploading: false})
                    message.error("Colorization Failed! Please try again!")
                    console.log(err)
                  })
            }
            console.log("uploaded")
        })
        .catch(err => {
          this.setState({uploading: false})
          message.error("Upload Failed! Please try again!")
          console.log(err)
        })
    }
    else {
      message.error("please select a valid image!")
    }
  }

  handleUrlSet = (e) => {
    this.setState({currentImage: e.target.value, activeTab:0})
  }

  handleInputBlur = (e) => {
    this.setState({activeTab:1})
  }

  componentWillMount() {
    ClientSession.isLoggedIn(isLoggedIn => {
      this.setState(
        {
          isLoggedIn: isLoggedIn,
        },
        () => {
          return "do nothing";
        }
      );
    });
  }

  render() {
    const { classes, ...rest } = this.props;
    return (
      <div>
      {!this.state.activeTab? <div>
        <Header
          color="transparent"
          routes={dashboardRoutes}
          brand="Colorize"
          rightLinks={<HeaderLinks />}
          fixed
          changeColorOnScroll={{
            height: 400,
            color: "white"
          }}
          {...rest}
        />
        <Parallax filter image={require("assets/img/landing-bg.jpg")}>
          <div className={classes.container}>
            <GridContainer>
              <GridItem xs={12} sm={12} md={12} style={{marginLeft: "auto", marginRight: "auto", alignContent:"center", justifyContent:"center" }}>
                <h1 className={classes.title}>Give color to your life.</h1>
                <h4>
                  You can colorize and customize your grayscale images with just a few clicks.
                </h4>
                <br />
                <input id="myInput" type="file" ref={(ref) => this.upload = ref} onChange={this.handleChangeFile} style={{ display: 'none' }} />
                  <Spin spinning={this.state.uploading}
                        indicator={<Icon type="loading" style={{ fontSize: 24 }} spin />} >
                          <Button
                          color="danger"
                          size="lg"
                          onClick={(e) => this.upload.click() }
                          >
                          <i className="fas fa-upload" />
                          Select a photo
                          </Button> 
                  </Spin>
                  <span style={{margin:"4px"}} color="primary"> or </span>
                <Input style={{color:"white"}} placeholder="enter a url" onChange={this.handleUrlSet} onBlur={this.handleInputBlur}/>
                  <p style={{marginLeft: "0", marginRight: "auto", maxWidth: "490px", fontSize: "80%", color: "rgba(255,255,255,0.7)", marginTop: "0", marginBottom: 0}}>
                  By uploading an image or URL you agree to our <a href="/tos" style={{fontWeight: "500", color: "rgba(255,255,255,0.7)"}} target="_blank" rel="noopener noreferrer">Terms of Service</a>.
                  This site is protected by reCAPTCHA and the Google
                  <a href="https://policies.google.com/privacy" style={{fontWeight: "500", color: "rgba(255,255,255,0.7)"}} target="_blank" rel="noopener noreferrer">Privacy Policy</a> and
                  <a href="https://policies.google.com/terms" style={{fontWeight: "500", color: "rgba(255,255,255,0.7)"}} target="_blank" rel="noopener noreferrer">Terms of Service</a> apply.
              }</p>
              </GridItem>
            </GridContainer>
          </div>
        </Parallax>
        <div className={classNames(classes.main, classes.mainRaised)}>
          <div className={classes.container}>
            {this.state.isLoggedIn?<ImageGallery />: <div style={{color:'black'}}>
              <br/><br/>
              <h3>
                Big news! you can also start using colorize from your android phone.
                Introducing the Colorize android app. Download Now!
              </h3>

              <small>Download the Android applicaion from the Google Play Store to use it on your phone</small>

              <MobileStoreButton
                store="android"
                url="https://play.google.com/store/apps/details?"
                style={{ width: "180" }}
              />
              <br/><br/>
              <br/><br/>
            </div>
            }
          </div>
        </div>
        <Footer />
      </div>:
      <div>
          <ColorizePage activeTab={this.state.activeTab} currentImage={this.state.currentImage} colorImage={this.state.colorImage}/>
      </div>
      }
      </div>
    );
  }
}

export default withStyles(landingPageStyle)(LandingPage);
