import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// @material-ui/icons

// core components
import Header from "components/Header/Header.jsx";
import Footer from "components/Footer/Footer.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import HeaderLinks from "components/Header/HeaderLinks.jsx";
import Parallax from "components/Parallax/Parallax.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";


import landingPageStyle from "assets/jss/material-kit-react/views/landingPage.jsx";

// Sections for this page
import ProductSection from "./Sections/ProductSection.jsx";
import TeamSection from "./Sections/TeamSection.jsx";
import WorkSection from "./Sections/WorkSection.jsx";
import { Input } from "@material-ui/core";
import ImageGallery from "../GalleryPage/GalleryPage.jsx";
import image from "assets/img/bg7.jpg";


const dashboardRoutes = [];

class Home extends React.Component {
  render() {
    const { classes, ...rest } = this.props;
    return (
      <div>
        <Header
          color="transparent"
          routes={dashboardRoutes}
          brand="Colorize"
          rightLinks={<HeaderLinks />}
          fixed
          changeColorOnScroll={{
            height: 400,
            color: "white"
          }}
          {...rest}
        />
        <div
          className={classes.pageHeader}
          style={{
            backgroundImage: "url(" + image + ")",
            backgroundSize: "cover",
            backgroundPosition: "top center"
          }}
        >
        <Parallax filter image={require("assets/img/landing-bg.jpg")}>
          <div className={classes.container}>
            <GridContainer style={{marginTop:"200px"}}>
              <GridItem xs={12} sm={12} md={12} style={{marginLeft: "auto", marginRight: "auto", alignContent:"center", justifyContent:"center" }}>
                <input id="myInput" type="file" ref={(ref) => this.upload = ref} style={{ display: 'none' }} />
                <Button
                color="danger"
                size="lg"
                onClick={(e) => this.upload.click() }
                >
                <i className="fas fa-upload" />
                Select a photo
                </Button> <span style={{margin:"4px"}} color="primary"> or </span>
                <Input style={{color:"white"}} placeholder="enter a url"/>
              </GridItem>
              <GridItem>
              <Card>
                    <CardBody>
                        <ImageGallery />
                    </CardBody>
                </Card>
              </GridItem>
            </GridContainer>
          </div>
        </Parallax>
          <Footer />
        </div>
      </div>
    );
  }
}

export default withStyles(landingPageStyle)(Home);
