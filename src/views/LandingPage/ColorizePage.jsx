import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import { Input } from "@material-ui/core";

// @material-ui/icons
import Camera from "@material-ui/icons/Camera";
import Palette from "@material-ui/icons/Palette";
import Share from "@material-ui/icons/Share";
// core components
import Header from "components/Header/Header.jsx";
import Footer from "components/Footer/Footer.jsx";
import Button from "components/CustomButtons/Button.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import HeaderLinks from "components/Header/HeaderLinks.jsx";
import NavPills from "components/NavPills/NavPills.jsx";
import Parallax from "components/Parallax/Parallax.jsx";

import { SketchPicker } from 'react-color';
import ImageEditor from '@toast-ui/react-image-editor';


import studio5 from "assets/img/examples/studio-5.jpg";
// import studio5gs from "assets/img/examples/studio-5.gs.jpg";

import profilePageStyle from "assets/jss/material-kit-react/views/profilePage.jsx";
import LModel from "../../services/api";
import ClientSession from "../../services/client-session";
import { message, Spin, Icon, Modal, Select } from "antd";
import { FacebookProvider, Initialize } from 'react-facebook';
import { TwitterShareButton, TelegramShareButton } from 'react-share';
import ImageEditorRc from 'react-cropper-image-editor';
import 'cropperjs/dist/cropper.css';
import 'assets/tui/dist/tui-image-editor.css'


var myTheme = {
  'common.bi.image': 'http://localhost:3000/favicon.ico',
  'common.bisize.width': '2px',
  'common.bisize.height': '2px',
  'common.backgroundImage': './img/bg.png',
  'common.backgroundColor': '#fff',
  'common.border': '1px solid #c1c1c1',

  // header
  'header.backgroundImage': 'none',
  'header.backgroundColor': 'transparent',
  'header.border': '0px',

  // load button
  'loadButton.backgroundColor': '#fff',
  'loadButton.border': '1px solid #ddd',
  'loadButton.color': '#222',
  'loadButton.fontFamily': '\'Noto Sans\', sans-serif',
  'loadButton.fontSize': '12px',

  // download button
  'downloadButton.backgroundColor': '#fdba3b',
  'downloadButton.border': '1px solid #fdba3b',
  'downloadButton.color': '#fff',
  'downloadButton.fontFamily': '\'Noto Sans\', sans-serif',
  'downloadButton.fontSize': '12px',

  // main icons
  'menu.normalIcon.path': 'tui/dist/svg/icon-d.svg',
  'menu.normalIcon.name': 'icon-d',
  'menu.activeIcon.path': 'tui/dist/svg/icon-b.svg',
  'menu.activeIcon.name': 'icon-b',
  'menu.disabledIcon.path': 'tui/dist/svg/icon-a.svg',
  'menu.disabledIcon.name': 'icon-a',
  'menu.hoverIcon.path': 'tui/dist/svg/icon-c.svg',
  'menu.hoverIcon.name': 'icon-c',
  'menu.iconSize.width': '24px',
  'menu.iconSize.height': '24px',

  // submenu primary color
  'submenu.backgroundColor': 'transparent',
  'submenu.partition.color': '#e5e5e5',

  // submenu icons
  'submenu.normalIcon.path': 'tui/dist/svg/icon-d.svg',
  'submenu.normalIcon.name': 'icon-d',
  'submenu.activeIcon.path': 'tui/dist/svg/icon-b.svg',
  'submenu.activeIcon.name': 'icon-b',
  'submenu.iconSize.width': '32px',
  'submenu.iconSize.height': '32px',

  // submenu labels
  'submenu.normalLabel.color': '#858585',
  'submenu.normalLabel.fontWeight': 'normal',
  'submenu.activeLabel.color': '#000',
  'submenu.activeLabel.fontWeight': 'normal',

  // checkbox style
  'checkbox.border': '1px solid #ccc',
  'checkbox.backgroundColor': '#fff',

  // rango style
  'range.pointer.color': '#333',
  'range.bar.color': '#ccc',
  'range.subbar.color': '#606060',

  'range.disabledPointer.color': '#d3d3d3',
  'range.disabledBar.color': 'rgba(85,85,85,0.06)',
  'range.disabledSubbar.color': 'rgba(51,51,51,0.2)',

  'range.value.color': '#000',
  'range.value.fontWeight': 'normal',
  'range.value.fontSize': '11px',
  'range.value.border': '0',
  'range.value.backgroundColor': '#f5f5f5',
  'range.title.color': '#000',
  'range.title.fontWeight': 'lighter',

  // colorpicker style
  'colorpicker.button.border': '0px',
  'colorpicker.title.color': '#000'
};



class Colorize extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      isLoggedIn: false,
      showAlbumSelectModal: false,
      albumId: "",
      albums: [],
      newAlbum: "",
      fileList: [],
      currentImage: this.props.currentImage? this.props.currentImage:"",
      colorImage: this.props.colorImage? this.props.colorImage: "",
      colorImageEdited: "",
      dataImage: "",
      savedImagePair: "",
      activeTab: this.props.activeTab? this.props.activeTab: 0,
      uploading: false
    }
  }

  componentDidMount() {
    ClientSession.isLoggedIn(isLoggedIn => {
      this.setState(
        {
          isLoggedIn: isLoggedIn,
        },
        () => {
          return "do nothing";
        }
      );
    });
  }

  componentWillReceiveProps(nextProps){
    if(this.state.currentImage !== nextProps.currentImage || this.state.colorImage !== nextProps.colorImage || this.state.activeTab !== nextProps.activeTab){
      this.setState({currentImage: nextProps.currentImage, activeTab: nextProps.activeTab, colorImage: nextProps.colorImage})
    }
  }

  handleChangeFile = (e) => {
    console.log(e.target.files)
    e.persist()
    ClientSession.getAuth((err, value)=>{
      let userId = value ? value.accountId: null
      if(e.target && e.target.files && e.target.files.length && e.target.files[0].type.startsWith('image')){
        console.log("uploading")
        this.setState({uploading:true,activeTab:0})
        LModel.upload('images', e.target.files)
        .then(response => {
          var files = response.data.result.files.data;
            if (files.length) {
              var file = files[0];
              let image = `${LModel.API_BASE_URL}Containers/${
                file.container
              }/download/${file.name}`;
              LModel.create('Colorizations/auto', {image:file.name})
                  .then( response2 => {
                    console.log(response2)
                    LModel.create('Images', {gray: image, color: response2.data.colorImage, accountId:userId})
                      .then( resp3 => {
                        this.setState({savedImagePair: resp3.data.id, currentImage: image, colorImage: response2.data.colorImage, activeTab:1, uploading: false})
                      }) .catch(err => {
                        this.setState({currentImage: image, colorImage: response2.data.colorImage, activeTab:1, uploading: false})
                        console.log(err)
                      })
                  }) .catch(err => {
                    this.setState({uploading: false})
                    message.error("Colorization Failed! Please try again!")
                    console.log(err)
                  })
                }
            console.log("uploaded")
        })
        .catch(err => {
          this.setState({uploading: false})
          message.error("Upload Failed! Please try again!")
          console.log(err)
        })
    }
    else {
      message.error("please select a valid image!")
    }
  })
  }

  handleUrlSet = (e) => {
    fetch(e.target.value)
    .then(r => r.blob())
    .then(
      blobFile => {
        let file = new File([blobFile], "newfile.png", { type: "image/png" })

        LModel.upload('images', [file])
        .then(response => {
          var files = response.data.result.files.data;
            if (files.length) {
              var file = files[0];
              let image = `${LModel.API_BASE_URL}Containers/${
                file.container
              }/download/${file.name}`;
              LModel.create('Colorizations/auto', {image:file.name})
              .then( response2 => {
                console.log(response2)
                LModel.create('Images', {gray: image, color: response2.data.colorImage})
                  .then( resp3 => {
                    this.setState({currentImage: image, colorImage: response2.data.colorImage, activeTab:1, uploading: false})
                  }) .catch(err => {
                    this.setState({currentImage: image, colorImage: response2.data.colorImage, activeTab:1, uploading: false})
                    console.log(err)
                  })
              }) .catch(err => {
                this.setState({uploading: false})
                message.error("Colorization Failed! Please try again!")
                console.log(err)
              })
            }
        })
    })
  }

  favoriteImage = (e) => {    
    e.preventDefault()
    if(this.state.isLoggedIn){
      ClientSession.getAuth((error, value)=>{
        LModel.create('Favorites', {imageId:this.state.savedImagePair, accountId: value.accountId})
          .then( resp => message.success("Favorited Image Sucessfully!"))
          .catch( err => message.error(err))
      })
    }
    else{
      let secondsToGo = 5;
      const modal = Modal.success({
        title: 'You need to login to favorite this colorization',
        content: `This modal will be closed after ${secondsToGo} second.`,
      });
      const timer = setInterval(() => {
        secondsToGo -= 1;
        modal.update({
          content: `This modal will be closed after ${secondsToGo} second.`,
        });
      }, 1000);
      setTimeout(() => {
        clearInterval(timer);
        modal.destroy();
      }, secondsToGo * 1000);
    }
  }

  addImage = (e) => {    
    e.preventDefault()
    if(this.state.isLoggedIn){
      ClientSession.getAuth((error, value)=>{
        LModel.find("Albums",null,`filter={"where":{"accountId":"${value.accountId}"}}`)
          .then( resp => {
            this.setState({showAlbumSelectModal: true, albums: resp.data})
          }).catch((err)=>message.error(err))
      })
    }
    else{
      let secondsToGo = 5;
      const modal = Modal.success({
        title: 'You need to login to favorite this colorization',
        content: `This modal will be closed after ${secondsToGo} second.`,
      });
      const timer = setInterval(() => {
        secondsToGo -= 1;
        modal.update({
          content: `This modal will be closed after ${secondsToGo} second.`,
        });
      }, 1000);
      setTimeout(() => {
        clearInterval(timer);
        modal.destroy();
      }, secondsToGo * 1000);
    }
  }

  handleAlbumChange = (e) => {
    console.log(e);
    
    this.setState({albumId: e})
  }
  handleInputAlbum = (e) => {
    this.setState({newAlbum: e.target.value})
  }

  createOrSelectAlbum = ()=>{
    if(this.state.newAlbum) {
      ClientSession.getAuth((error, value)=>{
        LModel.create('Albums', {name:this.state.newAlbum, accountId: value.accountId})
          .then( resp => {
            LModel.update("Images", this.state.savedImagePair, {albumId:resp.data.id})
              .then( resp => {
                message.success("Image added to album Successfully!")
                this.setState({showAlbumSelectModal:false})
              })
            })
          .catch( err => message.error(err))
      })
    }
    else if(this.state.albumId){
      LModel.update("Images", this.state.savedImagePair, {albumId:this.state.albumId})
        .then( resp => {
          message.success("Image added to album Successfully!")
          this.setState({showAlbumSelectModal:false})
        })
    }
  }

  onSaveImage = (image)=>{
    fetch(image)
    .then(r => r.blob())
    .then(
      blobFile => {
        let file = new File([blobFile], "newfile.png", { type: "image/png" })

        LModel.upload('images', [file])
        .then(response => {
          var files = response.data.result.files.data;
            if (files.length) {
              var file = files[0];
              let image = `${LModel.API_BASE_URL}Containers/${
                file.container
              }/download/${file.name}`;
        
              this.setState({colorImageEdited:image, dataImage: image, activeTab:2})     
            }
        })
    })
   
  } 

  render() {
    const { classes, ...rest } = this.props;
    const navImageClasses = classNames(classes.imgRounded, classes.imgGallery, classes.imgRaised);
    return (
      <div>
        <Header
          color="transparent"
          brand="Colorize"
          rightLinks={<HeaderLinks />}
          fixed
          changeColorOnScroll={{
            height: 200,
            color: "white"
          }}
          {...rest}
        />
        <Parallax small filter image={require("assets/img/profile-bg.jpg")} style={{height:'200px'}}/>
        <div className={classNames(classes.main, classes.mainRaised)}>
          <div>
            <div className={classes.container}>
              <GridContainer justify="center">
                <GridItem xs={12} sm={12} md={12} className={classes.navWrapper}>
                  <NavPills
                    alignCenter
                    color="primary"
                    active={this.state.activeTab}
                    tabs={[
                      {
                        tabButton: "Upload",
                        tabIcon: Camera,
                        tabContent: (
                          <GridContainer justify="center">
                            <GridItem xs={12} sm={12} md={8}>
                              <h4>Here you can upload your grayscale images and get them colorized. Note that the results may vary. While we try our best to make it work as best as we could, it may take time to colorize the image or it may fail to do so. Please be patient as we are improving the app.</h4>
                              <input id="myInput" type="file" ref={(ref) => this.upload = ref} onChange={this.handleChangeFile} style={{ display: 'none' }} />
                              <Spin spinning={this.state.uploading}
                                    indicator={<Icon type="loading" style={{ fontSize: 24 }} spin />} >
                                      <Button
                                      color="danger"
                                      size="lg"
                                      onClick={(e) => this.upload.click() }
                                      >
                                      <i className="fas fa-upload" />
                                      Select a photo
                                      </Button> 
                              </Spin>
                              <span style={{margin:"4px"}} color="primary"> or </span>
                              <Input style={{color:"black"}} placeholder="enter a url" onChange={this.handleUrlSet} onBlur={this.handleInputBlur}/>
                            </GridItem>
                          </GridContainer>
                        )
                      },
                      {
                        tabButton: "Colorize",
                        tabIcon: Palette,
                        tabContent: (
                          <GridContainer justify="center">
                            <GridItem xs={12} sm={12} md={4}>
                            <img
                                alt="..."
                                src={this.state.currentImage}
                                className={navImageClasses}
                            />
                              </GridItem>
                            
                            {/* <GridItem xs={12} sm={12} md={8}> */}
                              {/* <SketchPicker disableAlpha={true} color="#B6B337"/> */}
                              {this.state.colorImage?
                              <ImageEditor
                                includeUI={{
                                  loadImage: {
                                    path: this.state.colorImage,
                                    name: 'SampleImage'
                                  },
                                  theme: myTheme,
                                  menu: ['shape', 'filter', 'rotate', 'flip', 'crop','draw', 'icon', 'text', 'mask'],
                                  initMenu: 'rotate',
                                  uiSize: {
                                    width: '700px',
                                    height: '700px'
                                  },
                                  menuBarPosition: 'bottom'
                                }}
                                cssMaxHeight={600}
                                cssMaxWidth={700}
                                selectionStyle={{
                                  cornerSize: 20,
                                  rotatingPointOffset: 70
                                }}
                                usageStatistics={true}
                              />: 'nope'
                            }
                            {/* </GridItem> */}
                            {/* <GridItem xs={12} sm={12} md={4}>
                            <ImageEditorRc 
                              src={this.state.colorImage} 
                              className={navImageClasses}
                              autoCropArea={1}
                              movable={false}
                              saveImage={this.onSaveImage}
                            />

                            { <img
                              alt="..."
                              src={this.state.colorImage}
                              className={navImageClasses}
                            /> }
                            </GridItem> */}
                          </GridContainer>
                        )
                      },
                      {
                        tabButton: "Share",
                        tabIcon: Share,
                        tabContent: (
                          <GridContainer justify="center">
                          <GridItem xs={12} sm={12} md={8}>
                               <div className={classes.socialLine}>
                               <TwitterShareButton title="I can't believe this was a grayscale image" via="Colorize" hashtags={['colorize', 'color', 'Ethiopia']} style={{display:"inline"}} url={this.state.colorImageEdited?this.state.colorImageEdited:this.state.colorImage}>
                                <Button
                                  justIcon
                                  round
                                  href="#pablo"
                                  target="_blank"
                                  color="twitter"
                                  onClick={e => e.preventDefault()}
                                >
                                  <i className={"fab fa-twitter"} />
                                </Button>
                                </TwitterShareButton>
                                <FacebookProvider
                                    appId="221343595475964"
                                    >
                                    <Initialize>
                                    {({ isReady, api }) => {
                                       if(isReady){
                                           return (
                                            <Button
                                            justIcon
                                            round
                                            href="#pablo"
                                            target="_blank"
                                            color="facebook"
                                            onClick={e =>{
                                              e.preventDefault();
                                              api.ui({
                                                  method: 'share_open_graph',
                                                  action_type: 'og.shares',
                                                  action_properties: JSON.stringify({
                                                    object:{
                                                'og:url': this.state.colorImageEdited?this.state.colorImageEdited:this.state.colorImage,
                                                'og:title':"Colorize",
                                                'og:description':"Colize",
                                                'og:image': this.state.colorImageEdited?this.state.colorImageEdited:this.state.colorImage
                                                }
                                                  })
                                                })
                                          }}>
                                            <i className={"fab fa-facebook"} />
                                            </Button>
                                           )
                                       }
                                       else{
                                        return (
                                          <Button
                                            justIcon
                                            round
                                            href="#pablo"
                                            target="_blank"
                                            color="google"
                                            onClick={e => e.preventDefault()}
                                          >
                                            <i className={"fab fa-facebook"} />
                                          </Button>
                                             )
                                            }
                                         }}
                                         </Initialize>
                                </FacebookProvider>
                                {/* <Button
                                  justIcon
                                  round
                                  href="#pablo"
                                  target="_blank"
                                  color="google"
                                  onClick={e => e.preventDefault()}
                                >
                                  <i className={"fab fa-google"} />
                                </Button> */}
                              </div>
                              <p></p>
                              <div>
                                <img
                                  alt="..."
                                  src={this.state.colorImageEdited? this.state.colorImageEdited:this.state.colorImage}
                                  className={navImageClasses}
                                  style={{maxWidth:'335px'}}
                                />
                              </div>
                              <div className={classes.socialLine} style={{marginTop:"-90px", marginLeft:"-200px"}}>
                                <Button
                                    justIcon
                                    round
                                    href={this.state.colorImageEdited?this.state.colorImageEdited:this.state.colorImage}
                                    target="_blank"
                                    color="github"
                                    download
                                  >
                                    <i className={"fas fa-download"} />
                                  </Button>
                                      <Button
                                        justIcon
                                        round
                                        href="#pablo"
                                        target="_blank"
                                        color="danger"
                                        onClick={this.favoriteImage}
                                      >
                                        <i className={"fas fa-heart"} />
                                      </Button>
                                  <Button
                                    justIcon
                                    round
                                    href="#pablo"
                                    target="_blank"
                                    color="primary"
                                    onClick={this.addImage}
                                  >
                                    <i className={"fas fa-images"} />
                                  </Button>

                                </div>
                                <div>
                                  <p></p>
                                  <br/><br/>
                                  <p> By sharing the picture on the third party sites, you agree to adhere to the terms of services and privacy policies of the respective sites. </p>
                                  <p> This website is not liable for any consequences that may arise from sharing these pictures to the third party sites. </p>
                                  <p> We may not be held liable by the damage caused by these image, including but not limited to copywrite vaiolations. Please report any violations to webmaster@colorize.et</p>
                                </div>
                          </GridItem>
                        </GridContainer>
                        )
                      }
                    ]}
                  />
                </GridItem>
              </GridContainer>
            </div>
          </div>
        </div>
        <Footer />
        <Modal
          visible={this.state.showAlbumSelectModal}
          title="Add Colorization to Album"
          onCancel={()=>this.setState({showAlbumSelectModal:false})}
          onOk={this.createOrSelectAlbum}
          destroyOnClose={true}
        >
          <div  className='m-1'>
            Select an Album
          </div>
          <div className='m-1'>
          <Select
                onChange={this.handleAlbumChange}
                name="restaurantId"
                value={this.state.albumId}
                style={{ width: "100%" }}
                placeholder="Select an album..."
              >
                {this.state.albums.length ? (
                  ""
                ) : (
                  <Select.Option key={0}> No Albums Found!</Select.Option>
                )}
                {this.state.albums.map(album => {
                  return (
                    <Select.Option key={album.id} value={album.id}>
                      {album.name}
                    </Select.Option>
                  );
                })}
              </Select>
          </div>
          <br />
          <br />
          <div className='m-1'>
            or Create a new Album
          </div>
          <Input  className='m-1' name="newAlbum" placeholder="Album Name" value={this.state.newAlbum} onChange={this.handleInputAlbum} style={{width:'100%'}}></Input>
        </Modal>
      </div>
    );
  }
}

export default withStyles(profilePageStyle)(Colorize);
