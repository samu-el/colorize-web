import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Email from "@material-ui/icons/Email";
// import People from "@material-ui/icons/People";
// core components
import Header from "components/Header/Header.jsx";
import HeaderLinks from "components/Header/HeaderLinks.jsx";
import Footer from "components/Footer/Footer.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
// import CardHeader from "components/Card/CardHeader.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";

import loginPageStyle from "assets/jss/material-kit-react/views/loginPage.jsx";

import image from "assets/img/bg7.jpg";
import { FaceRounded } from "@material-ui/icons";
import { Checkbox } from "@material-ui/core";
import { message } from'antd';

import UserService from 'services/users.service.js'
import MobileStoreButton from "react-mobile-store-button";
import { TwitterShareButton, TelegramShareButton } from 'react-share'



class SignUpPage extends React.Component {
  constructor(props) {
    super(props);
    // we use this to make the card to appear after the page has been rendered
    this.state = {
      cardAnimaton: "cardHidden",
      name: "",
      email: "",
      password: "",
      tos:false
    };
  }
  componentDidMount() {
    // we add a hidden class to the card and after 700 ms we delete it and the transition appears
    setTimeout(
      function() {
        this.setState({ cardAnimaton: "" });
      }.bind(this),
      700
    );
  }
  
  handleSubmit = (e) => {
    e.preventDefault()
    console.log(e)
    if(!this.state.name) return message.error('Please enter your name!')
    if(!this.state.email) return message.error('Please enter your email!')
    if(!this.state.password) return message.error('Please enter your password!')
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(re.test(this.state.email))  message.error('Please enter a valid email address!');
    let data = { email: this.state.email, name: this.state.name, password: this.state.password }
    UserService.register(data)
      .then(response => {
        if(response.error){
          return message.error(response.message)
        }
        message.success(response.message)
        window.location = '#/login'
      }).catch( error => {
        message.error(error.message)
        console.log(error)
      })

  }

  render() {
    const { classes, ...rest } = this.props;
    return (
      <div>
        <Header
          absolute
          color="transparent"
          brand="Colorize"
          rightLinks={<HeaderLinks />}
          {...rest}
        />
        <div
          className={classes.pageHeader}
          style={{
            backgroundImage: "url(" + image + ")",
            backgroundSize: "cover",
            backgroundPosition: "top center"
          }}
        >
          <div className={classes.container} style={{paddingTop:"13vh"}}>
            <GridContainer justify="center" >
              <GridItem xs={12} sm={12} md={10} style={{marginBottom:"150px"}}>
                <Card className={classes[this.state.cardAnimaton]} justify="center">
                  <div style={{display:"flex", justifyContent:"center"}}>
                    <h2 style={{color: "#3C4858",
                                margin:"1.75rem 0 0.875rem",
                                marginTop: "30px",
                                minHeight: "32px",
                                fontWeight: "700",
                                fontFamily: "Roboto Slab, Times New Roman, serif",
                                textDecoration: "none",}}
                    >
                      Register
                    </h2>
                  </div>
                    <GridContainer justify="center" alignItems="stretch" >
                      <GridItem xs={12} sm={12} md={5}>
                        <CardBody style={{alignItems:"justify"}}>
                          <h3>
                            Colorize! Now!
                          </h3> 
                          <p>             
                          While you can use this app without registering it is best if you sign up so you can get the full potential of the app.
                          When logged in you can save your colorizations and tweak them to your likings. You can create albums and organize your pictures into these albums.
                          You can also favorite special pictures so you can access them quickly and easily. 
                          There is also the additional perk of sharing your pictures to social media with just a single click.
                          Register with colorize and be a part of the family.
                          </p>
                          <p>
                            Big news! you can also start using colorize from your android phone.
                            Introducing the Colorize android app. Download Now!
                          </p>

                          <MobileStoreButton
                            store="android"
                            url="https://play.google.com/store/apps/details?"
                            style={{ width: "180" }}
                          />
                        </CardBody>
                        <CardFooter>
                          Colorize
                        </CardFooter>
                      </GridItem>
                      <GridItem xs={12} sm={12} md={5}>
                      <p className={classes.divider} style={{marginBotton:"-40px"}}>Register using</p> 
                        <div className={classes.socialLine}>
                          <Button
                            justIcon
                            round
                            href="#pablo"
                            target="_blank"
                            color="twitter"
                            onClick={e => e.preventDefault()}
                          >
                            <i className={"fab fa-twitter"} />
                          </Button>
                          <Button
                            justIcon
                            round
                            href="#pablo"
                            target="_blank"
                            color="facebook"
                            onClick={e => e.preventDefault()}
                          >
                            <i className={"fab fa-facebook"} />
                          </Button>
                          <Button
                            justIcon
                            round
                            href="#pablo"
                            target="_blank"
                            color="google"
                            onClick={e => e.preventDefault()}
                          >
                            <i className={"fab fa-google"} />
                          </Button>
                        </div>
                        <form className={classes.form}>
                        <p className={classes.divider} style={{marginBotton:"-40px"}}>Or create account with us</p> 
                        <CardBody>
                          <CustomInput
                            labelText="Name"
                            id="first"
                            formControlProps={{
                              fullWidth: true
                            }}
                            inputProps={{
                              requried: "true",
                              type: "text",
                              value: this.state.name,
                              onChange: (e) => this.setState({name:e.target.value}),
                              endAdornment: (
                                <InputAdornment position="start">
                                  <FaceRounded className={classes.inputIconsColor} />
                                </InputAdornment>
                              )
                            }}
                          />
                          <CustomInput
                            labelText="Email"
                            id="email"
                            formControlProps={{
                              fullWidth: true
                            }}
                            inputProps={{
                              requried: "true",
                              type: "email",
                              value: this.state.email,
                              onChange: (e) => this.setState({email:e.target.value}),
                              endAdornment: (
                                <InputAdornment position="start">
                                  <Email className={classes.inputIconsColor} />
                                </InputAdornment>
                              )
                            }}
                          />
                          <CustomInput
                            labelText="Password"
                            id="pass"
                            formControlProps={{
                              fullWidth: true
                            }}
                            inputProps={{
                              requried: "true",
                              type: "password",
                              value: this.state.password,
                              onChange: (e) => this.setState({password:e.target.value}),
                              endAdornment: (
                                <InputAdornment position="start">
                                  <Icon className={classes.inputIconsColor}>
                                    lock_outline
                                  </Icon>
                                </InputAdornment>
                              )
                            }}
                          />
                          <div style={{margin:0}}>
                          <Checkbox onChange={e => this.setState({tos:e.target.checked})}/> I agree to the Terms and Conditions.
                          </div>
                        </CardBody>
                        <CardFooter style={{justifyContent:'center'}}>
                          <Button round={true} color="primary" size="lg" onClick={this.handleSubmit}>
                            Get started
                          </Button>
                        </CardFooter>
                        </form>
                      </GridItem>
                    </GridContainer>
                </Card>
              </GridItem>
            </GridContainer>
          </div>
          <Footer whiteFont />
        </div>
      </div>
    );
  }
}

export default withStyles(loginPageStyle)(SignUpPage);
