import React from "react";
import Loadable from "react-loadable";
import DefaultLayout from "./views/Layout/DefaultLayout";

function Loading() {
  return <div> Loading... </div>;
}

const Restaurants = Loadable({
  loader: () => import("./views/Base/Restaurants"),
  loading: Loading
});

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  {
    path: "/",
    exact: true,
    name: "Home",
    component: DefaultLayout
  },
]
export default routes;
