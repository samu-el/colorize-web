import React from "react";
import { HashRouter, Route, Switch } from "react-router-dom";

import LandingPage from "views/LandingPage/LandingPage.jsx";
import LoginPage from "views/LoginPage/LoginPage.jsx";
import SignUpPage from "./views/LoginPage/SignUpPage";
import ExternalRoute from './ExternalRoute'
import PrivateRoute from './PrivateRoute'
import Colorize from "./views/LandingPage/ColorizePage";
import FavoritesPage from "./views/GalleryPage/FavoritesPage";
import AlbumsPage from "./views/GalleryPage/AlbumsPage";


class App extends React.Component {
  
  render(){
    return(
      <HashRouter>
        <Switch>
          <ExternalRoute path="/login" component={LoginPage} />
          <ExternalRoute path="/signup" component={SignUpPage} />
          <PrivateRoute path="/favorites" component={FavoritesPage} />
          <PrivateRoute path="/albums" component={AlbumsPage} />
          <Route path='/colorize' component={Colorize} />
          <Route path="/" component={LandingPage} />
        </Switch>
      </HashRouter>
    )
  }

}

export default App;
