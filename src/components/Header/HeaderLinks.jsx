/*eslint-disable*/
import React from "react";
// react components for routing our app without refresh
import { Link } from "react-router-dom";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Tooltip from "@material-ui/core/Tooltip";

// @material-ui/icons
import { Apps, Lock, QuestionAnswer } from "@material-ui/icons";

// core components
import CustomDropdown from "components/CustomDropdown/CustomDropdown.jsx";
import Button from "components/CustomButtons/Button.jsx";

import headerLinksStyle from "assets/jss/material-kit-react/components/headerLinksStyle.jsx";
import ClientSession from 'services/client-session.js'
import UserService from 'services/users.service.js'

class HeaderLinks extends React.Component {
  constructor(props){
    super(props)

    this.state  = {
      isLoggedIn : false,
    }
  }

  componentWillMount() {
    ClientSession.isLoggedIn(isLoggedIn => {
      this.setState(
        {
          isLoggedIn: isLoggedIn,
        },
        () => {
          return "do nothing";
        }
      );
    });
  }

  favorites = () => {
    window.location = '#/favorites'
  }

  albums = () => {
    window.location = '#/albums'
  }

  logout = () => {
    UserService.logout()
    this.setState({isLoggedIn:false})
  }

  render(){
    const { classes } = this.props;
    return (
    <List className={classes.list} style={{width:"100%"}}>
      <List className={classes.list} style={{display:"flex", float:"left"}}>
        <ListItem className={classes.listItem}>
          <Button
            href="#/colorize"
            color="transparent"
            className={classes.navLink}
          >
            Colorize
          </Button>
        </ListItem>
        <ListItem className={classes.listItem}>
          <Button
            href="http://192.168.43.146:5000/explorer"
            color="transparent"
            className={classes.navLink}
            target='_blank'
          >
            API
          </Button>
        </ListItem>

        <ListItem className={classes.listItem}>
          <Button
            href="#"
            color="transparent"
            className={classes.navLink}
          >
            Pricing
          </Button>
        </ListItem>
        <ListItem className={classes.listItem}>
          <Button
            href="#"
            color="transparent"
            className={classes.navLink}
          >
            About
          </Button>
        </ListItem>
        {this.state.isLoggedIn?
        <span>
        <ListItem className={classes.listItem}>
          <CustomDropdown
              buttonText="Gallery"
              buttonProps={{
                className: classes.navLink,
                color: "transparent",
              }}
              dropdownList={[
                <span style={{width:'100%'}}
                  onClick={this.albums}
                >
                  Albums
                </span>,,
                {divider: true},
                <span style={{width:'100%'}}
                  onClick={this.favorites}
                >
                  Favorites
                </span>,
              ]}
            />
        </ListItem>
          <ListItem className={classes.listItem}>
            <CustomDropdown
                buttonText="My Account"
                buttonProps={{
                  className: classes.navLink,
                  color: "transparent",
                }}
                dropdownList={[
                  "Account Settings",
                  {divider: true},
                  <span style={{width:'100%'}}
                    onClick={this.logout}
                  >
                    Logout
                  </span>,
                ]}
              />
          </ListItem>
        </span>
      :
        <ListItem className={classes.listItem}>
          <Button
            href="#/login"
            color="transparent"
            className={classes.navLink}
          >
            <Lock className={classes.icons} /> Login
          </Button>
        </ListItem>
        }
      </List>
      <List className={classes.list} style={{display:"flex", float:"right"}}>
        <ListItem className={classes.listItem}>
          <Tooltip
            id="instagram-twitter"
            title="Follow us on twitter"
            placement={window.innerWidth > 959 ? "top" : "left"}
            classes={{ tooltip: classes.tooltip }}
          >
            <Button
              href="https://twitter.com"
                color="transparent"
              className={classes.navLink}
              target='_blank'
            >
              <i className={classes.socialIcons + " fab fa-twitter"} />
            </Button>
          </Tooltip>
        </ListItem>
        <ListItem className={classes.listItem}>
          <Tooltip
            id="instagram-facebook"
            title="Follow us on facebook"
            placement={window.innerWidth > 959 ? "top" : "left"}
            classes={{ tooltip: classes.tooltip }}
          >
            <Button
              color="transparent"
              href="https://facebook.com"
                className={classes.navLink}
                target='_blank'
            >
              <i className={classes.socialIcons + " fab fa-facebook"} />
            </Button>
          </Tooltip>
        </ListItem>
        <ListItem className={classes.listItem}>
          <Tooltip
            id="instagram-tooltip"
            title="Follow us on instagram"
            placement={window.innerWidth > 959 ? "top" : "left"}
            classes={{ tooltip: classes.tooltip }}
          >
            <Button
              color="transparent"
              href="https://instagram.com"
                className={classes.navLink}
                target='_blank'
            >
              <i className={classes.socialIcons + " fab fa-instagram"} />
            </Button>
          </Tooltip>
        </ListItem>
      </List>
    </List>
  );
  }
}

export default withStyles(headerLinksStyle)(HeaderLinks);
